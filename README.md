# Personnal Compiler
The program helps you to make a compiler from a custom language defined by yourself to some other language (like LaTeX or HTML), in the way you want it to be done.

## Recommendations
In order to help yourself, please consider making your custom language easy to read (something that looks like Markdown).

Also, there is a standard lgi library `data/lgifolders/standard`, you can use it either as an example, either as the base of your own *custom* folder.

## Requirements
You need to have Python3 installed.

## Installation
It is recommended to add this project to your PATH.

Simply add the following to `~/.bashrc` :
```
export PATH="<Path of the projetct's bin folder>:$PATH"
```

## How to use it
* Express as explained in the [documentation](https://github.com/UlysseDurand/compil_perso/blob/main/doc.md), in the `custom` folder, how you want to compile your custom language.
* Write your `document` file in your custom language.
* just run the following command :
```
compilperso -lgifolder custom -infile document -outfile output 
```
You should end up with a new `output` file.

### Example


Here is an example of what it can do, 
with *maths* as lgifolder and the following course.mdx file :
```
# GENERAL THEORY

## DETERMINANT IN SOME BASIS

### p-linear form on E.

The $\varphi:E^p \rightarrow \mathbb{K}$ application is said to be {\it p-linear}
if and only if each of its partial functions is linear.

### Alternated, antisymetric form

We will suppose that $\varphi$ is a p-linear form on E.

!tDefinition!

$\varphi$ is !balternated! if $\varphi$ is null on any system of p vectors which contains at least
two equal vectors. It is said to be !bantisymetric! if, when the S' system is the S system but
with two permutated vectors we have : $\varphi(S')=-\varphi(S)$.

!tProperties!
---i

#### $\varphi$ alternated $\implies \varphi$ antisymetric. true reciprocal if car $\mathbb{K}\neq 2$

#### $\varphi$ 
antisymetric
$\iff \forall \sigma \in S_p$, $\forall (x_1,\hdots,x_p) 
\in E^p, \varphi(x_{\sigma(1)},\hdots,x_{\sigma(p)})=\epsilon (\sigma).\varphi(x_1,\hdots,x_p)$.

#### $\varphi$ 
alternated
$\iff \forall (x_1,\hdots,x_p)\in E^p, ((x_1,\hdots,x_p)$
linearly dependant
$\implies \varphi (x_1,\hdots,x_p) = 0)$.
---i!

!tTheorem!
let E be a vector space of dimension n, and B be a basis of E. There exists a unique n-linear alternated form
$\varphi$ on E which evaluates to 1 on the B basis. By definition, $\varphi$ = det B.
Moreover, we deduce the following formula.
!equ!\forall x \in E, det_B(x_1,\hdots,x_n)=\sum_{\sigma \in \mathcal{S}} 
\epsilon (\sigma).\varphi_{\sigma(1)}(x_1)\hdots \varphi_{\sigma(n)}(x_n).!equ!
where $(\varphi_1,\hdots,\varphi_n)$ is the dual basis of $(e_1,\hdots,e_n)$.

!equ!e^{\sqrt(5)} = %
import math
print(math.exp(math.sqrt(5)),end='')
%!equ!
```
It produces the .tex file of [this pdf](example/linalg.pdf).